When you want to add a new Type ..
	1.	you add it to your datamodel.prisma
	2.	push it up (npm run deploy)
	3.	then you go into your own schema.graphql then create new functions in type mutations or query 
	4.	then match up mutations and queries by going into the Mutation.js or  Query.js  files and write resolvers. 
	>> createItem(parent, args, ctx, info) {}
	>> items(parent, args, ctx, info) {}

Our custom resolvers will have all our advance logic (charging credit cards, sending emails)
    
    schema.graphql is our public-facing API that our React application will interact with.
            You can find many helpful prisma queries in the prisma.graphql file


itemsConnection(where: ItemWhereInput): ItemConnection! === Pagination
skip: Int, first: Int === I want to skip the first 4 items but bring back 4 items in total
