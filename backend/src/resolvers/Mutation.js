// When someone wants to push data

const Mutations = {
    async createItem(parent, args, ctx, info) {
        // TODO: check if they are logged in
        // ctx = the context you are recieving from the request
        // db is how we expose the prisma database to ourselves
        // mutation gives us access to all the mutations we have
        // available to us in prisma.graphql (API)
        // info contains the query from the client side
        const item = await ctx.db.mutation.createItem({
            data: {
                ...args
            }
        }, info);

        return item;
    },
    updateItem(parent, args, ctx, info) {
        // first take a copy of the updates
        const updates = { ...args };
        // remove the ID from the updates
        delete updates.id;
        // run the update method 
        return ctx.db.mutation.updateItem(
            {
                data: updates,
                where: {
                    id: args.id,
                },
            },
        );
    },
    async deleteItem(parent, args, ctx, info) {
        const where = { id: args.id };
        // 1. find the item
        const item = await ctx.db.query.item({ where }, `{id title}`);
        // 2. check if they own that item, or have the permissions

        // 3. delete it!
        return ctx.db.mutation.deleteItem({ where }, info);

    }
};

module.exports = Mutations;
