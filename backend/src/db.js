//  Prisma Binding for GraphQl Yoga , Basically our controller just minus all the CRUD syntax
//  Helps us connect to Prisma db and run mutations 
//  All the things we were doing from the playground can now be run in our backend code with JS


// This file connects to the remote prisma DB and give us the abilities to query it with JS

const { Prisma } = require('prisma-binding');

const db = new Prisma({
    typeDefs: 'src/generated/prisma.graphql',
    endpoint: process.env.PRISMA_ENDPOINT,
    secret: process.env.PRISMA_SECRET,
    debug: false
});

module.exports = db;