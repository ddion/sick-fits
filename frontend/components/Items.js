import React, { Component } from 'react';
import {Query} from 'react-apollo';
import gql from 'graphql-tag';
import styled from 'styled-components';
import Item from './Item';
import Pagination from './Pagination';
import { perPage } from '../config';

// Best practice to write your Queries in All-Caps
// Also depending on the situation you can either write all your
// queries in a sperate file the import them where you need them
// or as Apollo recommends, write your queries in the files 
// where you're going to use them. If you need to access the 
// queries in other files export them and import where theyre needed
const ALL_ITEMS_QUERY = gql`
    query ALL_ITEMS_QUERY($skip: Int = 0, $first: Int = ${perPage}) {
        items(first: $first, skip: $skip, orderBy: createdAt_DESC) {
            id
            title
            price 
            description
            image
            largeImage
        }
    }
`;

const Center = styled.div`
    text-align: center;
`;

const ItemsList = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 60px;
    min-width: ${props => props.theme.maxWidth};
    margin: 0 auto;
`;

class Items extends Component {
    // The Query component takes a query prop itself
    // You pass the name of your query
    // The only child of a Query component must be a function
    render() {
        return (
            <Center>
                <Pagination page={this.props.page} />
                <Query query={ALL_ITEMS_QUERY}
                // fetchPolicy='network-only'
                variables={{
                    skip: this.props.page * perPage - perPage,
                    // first: perPage, Going to omit this as its stated in the query at the top of the page
                }} >
                    {({ data, error, loading }) => {
                        // console.log(data);
                        if(loading) return <p>Loading...</p>;
                        if(error) return <p>Error {error.message}</p>;
                        return (
                        <ItemsList>
                             {data.items.map(item => <Item item={item} key={item.id}/>)}
                        </ItemsList>
                        );
                    }}
                </Query>
               <Pagination page={this.props.page} />
            </Center>
        );
    }
}

// So depending on the time of when ever code is being coded
// You can either use RenderProps or HighOrder Components to
// Query your stuff??
export default Items;
export { ALL_ITEMS_QUERY };