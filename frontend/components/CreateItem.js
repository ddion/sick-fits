import React, { Component } from 'react';
import {Mutation} from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import Form from './styles/Form';
import formatMoney from '../lib/formatMoney';
import Error from './ErrorMessage';

// Mutations take in arguments first
// then the args become available as variables
// Then with the $ you say take the variable of the 
// exact same name and from that query return an ID
const CREATE_ITEM_MUTATION = gql`
    mutation CREATE_ITEM_MUTATION(
                $title: String!
                $price: Int!
                $description: String!
                $image: String
                $largeImage: String
    ) {
            createItem (
                title: $title
                price: $price 
                description: $description
                image: $image
                largeImage: $largeImage
                ) {
                    id
                }
        }
`;


class CreateItem extends Component {
    state = {
        title: 'Cool Shoes',
        description: 'Daniels back at it',
        image: '',
        largeImage: '',
        price: 1000,
    };
    handleChange = (e) => {
        const {name, type, value} = e.target;
        const val = type === 'number' ? parseFloat(value) : value;
        this.setState({
            [name]: val
        })
    };
   
   uploadFile = async e => {
        console.log('Uploading File....');
        const files = e.target.files;
        const data = new FormData();
        data.append('file', files[0]);
        data.append('upload_preset', 'sickfits');

        const res = await fetch('https://api.cloudinary.com/v1_1/dslr75rpk/image/upload', {
            method: 'POST',
            body: data
        });
        // convert respose you get above into JSON
        const file = await res.json();
        console.log(file);
        this.setState({
            image: file.secure_url,
            largeImage: file.eager[0].secure_url
        })
    };
    render() {
        return (
        
        <Mutation 
            mutation={CREATE_ITEM_MUTATION}
            variables={this.state}>
            {( createItem, {loading, error}) => (
            <Form onSubmit={async e => {
                // Form onSubmit prevents state going into the url
                e.preventDefault();
                // Call the mutation
                const res = await createItem();
                // change them to the single item page
                console.log(res);
                Router.push({
                    pathname: '/item',
                    query: { id: res.data.createItem.id }
                })
            }}>
            <Error error={error}/>
            {/* fieldset lading makes it so the user can't 
        edit the form or click submit again while it's still loading in the DB */}
            <fieldset disabled={loading} aria-busy={loading}>
            {/* Apollo flips the loading switch on and off */}
            
                <label htmlFor="file">
                Image
                <input 
                type="file" 
                id="file" 
                name="file" 
                placeholder="Upload an image" 
                required 
                // value={this.state.image}
                onChange={this.uploadFile}/>
                {this.state.image ? (<img width="150" src={this.state.image} alt="Upload Preview"/>) : <p>Upload Preview</p>}
                </label>

                <label htmlFor="title">
                Title
                <input 
                type="text" 
                id="title" 
                name="title" 
                placeholder="Title" 
                required 
                value={this.state.title}
                onChange={this.handleChange}/>
                </label>

                <label htmlFor="price">
                Price
                <input 
                type="number" 
                id="price" 
                name="price" 
                placeholder="Price" 
                required 
                value={this.state.price}
                onChange={this.handleChange}/>
                </label>

                <label htmlFor="description">
                Description
                <textarea 
                id="description" 
                name="description" 
                placeholder="Enter a description" 
                required 
                value={this.state.description}
                onChange={this.handleChange}
                />
                </label>
                <button type="submit" 
                //   onClick={this.handleClick}
                >Submit</button>
            </fieldset>
            </Form>
            )}
            </Mutation> 
        )
    }
}

export default CreateItem;
export { CREATE_ITEM_MUTATION };