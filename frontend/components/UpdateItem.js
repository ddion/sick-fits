import React, { Component } from 'react';
import {Mutation, Query} from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';
import Form from './styles/Form';
import formatMoney from '../lib/formatMoney';
import Error from './ErrorMessage';

// Mutations take in arguments first
// then the args become available as variables
// Then with the $ you say take the variable of the 
// exact same name and from that query return an ID
const SINGLE_ITEM_QUERY = gql `
    query SINGLE_ITEM_QUERY($id: ID!) {
        item(where: {id: $id}) {
            id
            title
            description
            price
        }
    }
`;

const UPDATE_ITEM_MUTATION = gql`
    mutation UPDATE_ITEM_MUTATION(
                $id: ID!,
                $title: String,
                $price: Int,
                $description: String
    ) {
            updateItem (
                id: $id
                title: $title
                price: $price 
                description: $description
                ) {
                    id
                    title
                    description
                    price
                }
        }
`;


class UpdateItem extends Component {
    state = { };
    handleChange = (e) => {
        const {name, type, value} = e.target;
        const val = type === 'number' ? parseFloat(value) : value;
        this.setState({
            [name]: val
        })
    };
    updateItem = async (e, updateItemMutation) => {
        e.preventDefault();
        console.log('Updating Item!!');
        console.log(this.state);
        const res = await updateItemMutation({
            variables: {
                id: this.props.id,
                ...this.state,
            },
        });
        console.log('Updated!!');
    };
   
    render() {
        return (
        <Query 
            query={SINGLE_ITEM_QUERY}
            variables={{id: this.props.id}}>
            {({data, loading}) => {
                if(loading) return <p>Loading...</p>;
                if(!data.item) return <p>No Item Found for ID {this.props.id}</p>
                return (
                
        <Mutation 
            mutation={UPDATE_ITEM_MUTATION}
            variables={this.state}>
            {( updateItem, {loading, error}) => (
            <Form onSubmit={e => this.updateItem(e, updateItem)}>
            <Error error={error}/>
            {/* fieldset lading makes it so the user can't 
        edit the form or click submit again while it's still loading in the DB */}
            <fieldset disabled={loading} aria-busy={loading}>
            {/* Apollo flips the loading switch on and off */}
                <label htmlFor="title">
                Title
                <input 
                type="text" 
                id="title" 
                name="title" 
                placeholder="Title" 
                required 
                // defaultValue allows us to set some defult text
                // without tying it to the State
                defaultValue={data.item.title}
                onChange={this.handleChange}/>
                </label>

                <label htmlFor="price">
                Price
                <input 
                type="number" 
                id="price" 
                name="price" 
                placeholder="Price" 
                required 
                defaultValue={data.item.price}
                onChange={this.handleChange}/>
                </label>

                <label htmlFor="description">
                Description
                <textarea 
                id="description" 
                name="description" 
                placeholder="Enter a description" 
                required 
                defaultValue={data.item.description}
                onChange={this.handleChange}
                />
                </label>
                <button type="submit" 
                //   onClick={this.handleClick}
                >Sav{loading ? 'ing' : 'e' } Changes</button>
            </fieldset>
            </Form>
            )}
            </Mutation> 
            )
        }}
    </Query>
        )
    }
}

export default UpdateItem;
export { UPDATE_ITEM_MUTATION };