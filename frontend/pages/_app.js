import App, { Container } from 'next/app';
// This `App` component wraps everything up for easy prop transfer
// Good for keeping state when navigating pages
import Page from '../components/Page';
import { ApolloProvider } from 'react-apollo';
import withData from '../lib/withData';

class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        // For every single page we have this function is 
        // going to crawl the page for queries or mutations 
        // we have inside of that page that need to be fetched.
        // crawl for data, fetch data, return data
        let pageProps = {};
        if(Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        // This exposes the query to the user
        // exposes the query to every page
        pageProps.query = ctx.query;
        return { pageProps };
    }
    // Is not necessary for Client Side projects, just server side projects
    // You can find example code like this on NextJs site
    render() {
        const { Component, apollo, pageProps } = this.props;
    

    return (
        <Container>
            <ApolloProvider client={apollo}>
            <Page>
                <Component {...pageProps} />
            </Page>
            </ApolloProvider>
        </Container>
    )
}
}

export default withData(MyApp);