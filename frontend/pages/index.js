// import React from 'react'; 
// Don't actually need the line above because `NextJs` will do that for us

// class Home extends React.Component {
//     render() {
//         return <p>Hey!</p>
//     }
// }

import Items from '../components/Items';

const Home = props => (
    <div>
        <Items page={parseFloat(props.query.page) || 1} />
    </div>
)

export default Home;