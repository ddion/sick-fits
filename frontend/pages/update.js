import UpdateItem from '../components/UpdateItem';
//  in _app.js we exposed the query so now you just need to pass down
// the id through UpdateItem component

const Sell = ({ query }) => (
    <div>
        <UpdateItem id={query.id} />
    </div>
)

export default Sell;